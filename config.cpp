class CfgPatches
{
	class dzr_replace_corpse
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_replace_corpse
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_replace_corpse";
		name = "dzr_replace_corpse";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_replace_corpse/4_World"};
			};
		};
	};
};